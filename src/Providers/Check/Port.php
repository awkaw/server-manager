<?php

namespace ServerManager\Providers\Check;

use ServerManager\Contracts\CheckProvider;
use ServerManager\Model\Watch;

class Port implements CheckProvider
{
    public function handle(Watch $watch): bool
    {
        $result = false;

        try{

            $handle = fsockopen($watch->getHost(), $watch->getPort());

            if ($handle !== false) {

                $result = true;

                fclose($handle);
            }

        }catch (\Exception $e){

        }

        return $result;
    }
}