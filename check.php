#!/usr/bin/php
<?php

require_once __DIR__."/vendor/autoload.php";

\ServerManager\Logger::setDir(__DIR__);

$config = json_decode(file_get_contents(__DIR__."/config.json"), true);

$manager = new \ServerManager\ServerManager($config);

$manager->checkAll();