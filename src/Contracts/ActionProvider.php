<?php

namespace ServerManager\Contracts;

use ServerManager\Model\Watch;

interface ActionProvider
{
    public function exec(Watch $watch): void;
}