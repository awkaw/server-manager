<?php

namespace ServerManager\Model;

class Watch
{
    protected $name = "";
    protected $host = "";
    protected $port = 0;
    protected $command = "";

    public function getName(): string{
        return $this->name;
    }

    public function getHost(): string{
        return $this->host;
    }

    public function getPort(): string{
        return $this->port;
    }

    public function getCommand(): string{
        return $this->command;
    }

    public function setName(string $name): void{
        $this->name = $name;
    }

    public function setHost(string $host): void{
        $this->host = $host;
    }

    public function setPort(int $port): void{
        $this->port = $port;
    }

    public function setCommand(string $command): void{
        $this->command = $command;
    }
}