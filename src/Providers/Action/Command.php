<?php

namespace ServerManager\Providers\Action;

use ServerManager\Contracts\ActionProvider;
use ServerManager\Logger;
use ServerManager\Model\Watch;

class Command implements ActionProvider
{
    public function exec(Watch $watch): void
    {
        $command = $watch->getCommand();

        shell_exec($command." >> ".Logger::getFilename());

        Logger::toLog("command: {$command}");
    }
}