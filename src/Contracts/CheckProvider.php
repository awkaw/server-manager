<?php

namespace ServerManager\Contracts;

use ServerManager\Model\Watch;

interface CheckProvider
{
    public function handle(Watch $project): bool;
}