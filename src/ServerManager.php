<?php

namespace ServerManager;

use ServerManager\Contracts\CheckProvider;
use ServerManager\Model\Watch;
use ServerManager\Providers\Action\Command;
use ServerManager\Providers\Check\Port;

class ServerManager
{
    const CHECK_PROVIDERS = [
        Port::class
    ];

    protected $config;
    protected $watch = [];

    public function __construct(array $config)
    {
        ini_set("display_errors", 0);
        error_reporting(0);

        $this->config = $config;

        if(isset($config['watch']) && is_array($config['watch'])){
            $this->watch = $config['watch'];
        }
    }

    public function checkAll(){

        if(is_array($this->watch) && !empty($this->watch)){

            foreach ($this->watch as $watch) {

                $watchObj = new Watch();

                if(isset($watch["name"])){
                    $watchObj->setName($watch["name"]);
                }

                if(isset($watch["host"])){
                    $watchObj->setHost($watch["host"]);
                }

                if(isset($watch["port"])){
                    $watchObj->setPort((int)$watch["port"]);
                }

                if(isset($watch["command"])){
                    $watchObj->setCommand($watch["command"]);
                }

                foreach (self::CHECK_PROVIDERS as $provider) {

                    $providerObj = new $provider();

                    if($providerObj instanceof CheckProvider){

                        if(!$providerObj->handle($watchObj)){
                            (new Command())->exec($watchObj);
                        }
                    }
                }
            }
        }
    }
}