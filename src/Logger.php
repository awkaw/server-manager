<?php

namespace ServerManager;

class Logger
{
    private static $dir = null;
    private static $filename = "server.log";

    public static function getFilename(){

        if(is_null(self::$dir)){
            self::$dir = __DIR__."/..";
        }

        return self::$dir."/".self::$filename;
    }

    public static function setDir(string $dir){
        self::$dir = $dir;
    }

    public static function toLog($obj){

        if(is_array($obj)){
            $obj = print_r($obj, true);
        }

        $obj = "\n".date("Y-m-d H:i:s").": ".$obj;

        file_put_contents(self::getFilename(), $obj, FILE_APPEND);
    }
}